## Description

An implementation of a small subset of the capabilities of the Hacker News Algolia API. New Items are retrieved every hour from the main HackerNews API.

## Installation

Local

```bash
$ cp .env.example .env
$ npm install
```

Docker container

```bash
# Bundled with a postgresql database
$ cp .env.example .env
$ docker compose -f "docker-compose.yml" up --build
```

## Running the app

```bash
# docker
$  docker compose -f "docker-compose.yml" up

# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

## Notes

- the default user credentials created by the seeder for the /login endpoint are

```json
{
  "email": "example@reign.cl",
  "password": "password"
}
```

- The API Docs are available at /api/docs
- One thing I'd have liked to do would be to implement the complex tags filtering from the Algolia API, I did not have enough time this weekend.
- An Online version of this API is available at my personal site https://api.servinges.cl because I wanted to demostrate that I have some knowledge about web servers configuration (nginx), ec2, web hosting, certificates, etc.

