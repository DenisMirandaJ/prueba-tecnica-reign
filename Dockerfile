FROM node:16.16
WORKDIR /app
COPY . .    
RUN rm -r .env
RUN npm ci --only=production
EXPOSE 3000
RUN npm run prebuild
RUN npm run build
CMD ["node", "./dist/main.js" ]