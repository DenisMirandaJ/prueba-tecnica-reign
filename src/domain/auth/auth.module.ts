import { AuthService } from './providers/auth.service';
import { AuthController } from './controllers/auth.controller';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from './entities/user.entity';
import { UserRepositoryService } from './repositories/users.repository';
import { JwtModule } from '@nestjs/jwt';

@Module({
  imports: [
    TypeOrmModule.forFeature([User]),
    JwtModule.register({
      signOptions: { expiresIn: '14 days' },
    }),
  ],
  controllers: [AuthController],
  providers: [AuthService, UserRepositoryService],
  exports: [AuthService],
})
export class AuthModule {}
