import { Inject, Injectable } from '@nestjs/common';
import { Timeout } from '@nestjs/schedule';
import { InjectRepository } from '@nestjs/typeorm';
import { WINSTON_MODULE_PROVIDER } from 'nest-winston';
import { Logger, Repository } from 'typeorm';
import { User } from '../entities/user.entity';

@Injectable()
export class UserRepositoryService {
  constructor(
    @InjectRepository(User)
    private usersRepository: Repository<User>,
    @Inject(WINSTON_MODULE_PROVIDER)
    private readonly logger: Logger,
  ) {}

  async findUserByEmail(email: string): Promise<User> {
    return this.usersRepository.findOneBy({ email }); 
  }

  @Timeout(0)
  async seedUsers() {
    const countUsers = await this.usersRepository.count();

    if (countUsers === 0) {
      this.logger.log('info', 'Empty users table, seeding example records');
      await this.usersRepository.insert({
        email: 'example@reign.cl',
        password:
          '$argon2i$v=19$m=16,t=2,p=1$Q0d4OVh4bWRHbFpXajY1eg$uziszlGY5nBo9iSWXNo1xg',
      });
    }
  }
}
