import { Test } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { User } from '../entities/user.entity';
import { UserRepositoryService } from '../repositories/users.repository';
// eslint-disable-next-line @typescript-eslint/no-var-requires
require('dotenv').config({ path: '.env' });

describe('UserRepository', () => {
  let userRepository: UserRepositoryService;

  const existingEmail = 'example@reign.cl';
  const examplePassword =
    '$argon2i$v=19$m=16,t=2,p=1$Q0d4OVh4bWRHbFpXajY1eg$uziszlGY5nBo9iSWXNo1xg';

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      providers: [
        UserRepositoryService,
        {
          provide: getRepositoryToken(User),
          useValue: {
            findOneBy: jest.fn(({ email }) => {
              if (email === existingEmail) {
                return {
                  email: existingEmail,
                  password: examplePassword,
                };
              }

              return null;
            }),
          },
        },
      ],
    }).compile();

    userRepository = moduleRef.get<UserRepositoryService>(
      UserRepositoryService,
    );
  });

  it('should be defined', () => {
    expect(userRepository).toBeDefined();
  });

  describe('findUserByEmail', () => {
    it('should be defined', () => {
      expect(userRepository.findUserByEmail).toBeDefined();
    });

    it('should found user if provided existing email', async () => {
      const user = await userRepository.findUserByEmail(existingEmail);
      expect(user).toBeDefined();
      expect(user).not.toBe(null);
    });

    it('should return null user if provided non existing email', async () => {
      const user = await userRepository.findUserByEmail('email@does-not-exist');
      expect(user).toBe(null);
    });
  });
});
