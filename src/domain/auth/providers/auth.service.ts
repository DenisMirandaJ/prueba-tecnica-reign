import {
  Injectable,
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common';
import * as argon2 from 'argon2';
import * as jsonwebtoken from 'jsonwebtoken';
import { UserRepositoryService } from '../repositories/users.repository';
import { JwtService } from '@nestjs/jwt';
import { LoginResponse } from '../interfaces/auth.types';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class AuthService {
  constructor(
    private readonly usersRepository: UserRepositoryService,
    private readonly jwtService: JwtService,
    private readonly configService: ConfigService,
  ) {}

  public async login(email: string, password: string): Promise<LoginResponse> {
    const user = await this.usersRepository.findUserByEmail(email);

    if (!user) {
      throw new NotFoundException('User Not Found');
    }

    const doesPasswordsMatch = await argon2.verify(user.password, password);

    if (!doesPasswordsMatch) {
      throw new UnauthorizedException('Invalid Password');
    }
    const jwt = this.jwtService.sign(
      {
        userId: user.id,
        userEmail: user.email,
      },
      { secret: this.configService.get<string>('JWT_SECRET') },
    );

    return {
      token: jwt,
    };
  }

  public checkJwt(jwt: string): boolean | 'expired' {
    if (!jwt) {
      return false;
    }

    try {
      this.jwtService.verify(jwt, {
        secret: this.configService.get<string>('JWT_SECRET'),
      });
      return true;
    } catch (error) {
      if (error instanceof jsonwebtoken.TokenExpiredError) {
        return 'expired';
      }
      return false;
    }
  }
}
