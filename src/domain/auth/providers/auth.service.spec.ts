import { Test } from '@nestjs/testing';
import { AuthService } from './auth.service';
import * as jwt from 'jsonwebtoken';
import { getRepositoryToken } from '@nestjs/typeorm';
import { NotFoundException, UnauthorizedException } from '@nestjs/common';
import { AuthController } from '../controllers/auth.controller';
import { User } from '../entities/user.entity';
import { throwsSpecificException } from '../../shared/utils/test.utils';
import { JwtService } from '@nestjs/jwt';
import { UserRepositoryService } from '../repositories/users.repository';
import { ConfigService } from '@nestjs/config';
// eslint-disable-next-line @typescript-eslint/no-var-requires
require('dotenv').config({ path: '.env' });

describe('AuthService', () => {
  let authService: AuthService;

  const existingEmail = 'example@reign.cl';
  const examplePassword =
    '$argon2i$v=19$m=16,t=2,p=1$Q0d4OVh4bWRHbFpXajY1eg$uziszlGY5nBo9iSWXNo1xg';

  const testJWTSEcret = process.env.JWT_SECRET;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      controllers: [AuthController],
      providers: [
        AuthService,
        UserRepositoryService,
        JwtService,
        ConfigService,
        {
          provide: getRepositoryToken(User),
          useValue: {
            findOneBy: jest.fn(({ email }) => {
              if (email === existingEmail) {
                return {
                  email: existingEmail,
                  password: examplePassword,
                };
              }

              return null;
            }),
          },
        },
      ],
    }).compile();

    authService = moduleRef.get<AuthService>(AuthService);
  });

  describe('login', () => {
    it('should be defined', () => {
      expect(authService).toBeDefined();
      expect(authService.login).toBeDefined();
    });

    it('should return valid auth token', async () => {
      const authToken = await authService.login(existingEmail, 'password');
      let tokenVerified = false;

      try {
        jwt.verify(authToken?.token, process.env.JWT_SECRET);
        tokenVerified = true;
      } catch (error) {
        tokenVerified = false;
      }
      expect(tokenVerified).toBe(true);
    });

    it('should fail on non existing user with NotFoundException', async () => {
      const throwsNotFoundException = await throwsSpecificException(
        () => authService.login('user@does-not-exist.com', 'password'),
        NotFoundException,
      );
      expect(throwsNotFoundException).toBe(true);
    });

    it('should fail on wrong credentials with InvalidCredentialsException', async () => {
      const throwsUnauthorizedException = await throwsSpecificException(
        () => authService.login(existingEmail, 'invalid-password'),
        UnauthorizedException,
      );
      expect(throwsUnauthorizedException).toBe(true);
    });
  });

  describe('check-jwt', () => {
    it('should be defined', () => {
      expect(authService.checkJwt).toBeDefined();
    });

    const validJwtToken = jwt.sign({}, testJWTSEcret);
    const invalidToken = jwt.sign({}, 'invalid-secret');
    const expiredJwtToken = jwt.sign({}, testJWTSEcret, {
      expiresIn: '-10s',
    });

    it('should accept valid jwt token', async () => {
      expect(authService.checkJwt(validJwtToken)).toBe(true);
    });

    it('should reject empty jwt token', async () => {
      expect(authService.checkJwt(undefined)).toBe(false);
    });

    it('should reject invalid jwt token', async () => {
      expect(authService.checkJwt(invalidToken)).toBe(false);
    });

    it('should reject expired jwt token', async () => {
      expect(authService.checkJwt(expiredJwtToken)).toBe('expired');
    });
  });
});
