import { Injectable, Optional } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, SelectQueryBuilder, UpdateQueryBuilder } from 'typeorm';
import { PaginatedResponse } from '../../shared/interfaces/interfaces';
import {
  dateToUnixTimestamp,
  getDateRangeByMonthName,
} from '../../shared/utils/date.utils';
import {
  QueryFiltersDto,
  ShowHiddenRecordsOptionsDto,
  VisibilityQueryFiltersDto,
} from '../dto/newsFilters.dto';
import { HackerNews } from '../entities/news.entity';
import { NewsLoadTimes } from '../entities/newsLoadTimes.entity';
import { HackerNewsApiWrapper } from '../providers/hackerNewsApiWrapper.service';

@Injectable()
export class NewsRepositoryService {
  constructor(
    private hackerNewsApiWrapper: HackerNewsApiWrapper,
    @InjectRepository(HackerNews)
    private hackerNewsRepository: Repository<HackerNews>,
    @InjectRepository(NewsLoadTimes)
    @Optional()
    private newsLoadTimesRepository: Repository<NewsLoadTimes>,
  ) {}

  async saveRecentNews() {
    let minDate: Date | undefined;

    const lastLoadTimes = await this.newsLoadTimesRepository?.find({
      order: {
        timeStamp: 'DESC',
      },
      take: 1,
    });

    const lastTimeNewswereLoaded = lastLoadTimes?.[0];

    // if news are going to be loaded for the fisrt time
    if (!lastTimeNewswereLoaded) {
      minDate = new Date(0);
      await this.hackerNewsRepository.clear();
    } else {
      minDate = lastTimeNewswereLoaded.timeStamp;
    }

    this.hackerNewsApiWrapper.setParams({
      createdAtUnixEpochInSeconds: minDate
        ? dateToUnixTimestamp(minDate)
        : undefined,
      query: 'nodejs',
    });

    while (true) {
      const response = await this.hackerNewsApiWrapper.getApiResponse().next();
      this.hackerNewsRepository.insert(response.value.hits);
      if (response.done) {
        break;
      }
    }

    await this.newsLoadTimesRepository.insert({
      timeStamp: new Date(),
    });
  }

  async findMany(
    filters: QueryFiltersDto,
  ): Promise<PaginatedResponse<HackerNews>> {
    let query = this.hackerNewsRepository.createQueryBuilder('hacker_news');

    query = this.addDateRangeFilterToQuery(
      query,
      filters,
    ) as SelectQueryBuilder<HackerNews>;

    query = this.addAuthorFilterToQuery(
      query,
      filters,
    ) as SelectQueryBuilder<HackerNews>;

    query = this.addTagsFilterToQuery(
      query,
      filters,
    ) as SelectQueryBuilder<HackerNews>;

    query = query
      .where('hacker_news.hidden = false')
      .skip((filters.page - 1) * filters.items_per_page)
      .take(filters.items_per_page);

    const result = await query.getMany();

    const totalHackerNews = await this.hackerNewsRepository.count({
      where: { hidden: false },
    });

    return {
      items: result,
      page: filters.page,
      items_per_page: filters.items_per_page,
      number_of_pages: Math.ceil(totalHackerNews / filters.items_per_page) + 1,
    };
  }

  //Shows or hiden news based onFilters
  async changeNewsVisibility(
    filters: VisibilityQueryFiltersDto,
    hide: 'hide' | 'show',
  ) {
    let query = this.hackerNewsRepository
      .createQueryBuilder('hacker_news')
      .update()
      .set({ hidden: hide === 'hide' });

    query = this.addDateRangeFilterToQuery(
      query,
      filters as QueryFiltersDto,
    ) as UpdateQueryBuilder<HackerNews>;

    query = this.addAuthorFilterToQuery(
      query,
      filters as QueryFiltersDto,
    ) as UpdateQueryBuilder<HackerNews>;

    query = this.addTagsFilterToQuery(
      query,
      filters as QueryFiltersDto,
    ) as UpdateQueryBuilder<HackerNews>;

    const result = await query.execute();

    return { updatedRows: result.affected };
  }

  async getHiddenRecords(filters: ShowHiddenRecordsOptionsDto) {
    let query = this.hackerNewsRepository.createQueryBuilder('hacker_news');

    query = query
      .where('hacker_news.hidden = true')
      .skip((filters.page - 1) * filters.items_per_page)
      .take(filters.items_per_page);

    const result = await query.getMany();

    const totalHiddenNews = await this.hackerNewsRepository.count({
      where: { hidden: true },
    });

    return {
      page: filters.page,
      total_pages: Math.ceil(totalHiddenNews / filters.items_per_page),
      items_per_page: filters.items_per_page,
      items: result,
    };
  }

  private addDateRangeFilterToQuery(
    query: SelectQueryBuilder<HackerNews> | UpdateQueryBuilder<HackerNews>,
    filters?: QueryFiltersDto,
  ) {
    let dateRange: { minDate: number; maxDate: number };

    if (filters.month_word) {
      const monthWordDateRange = getDateRangeByMonthName(filters.month_word);
      dateRange = {
        minDate: dateToUnixTimestamp(monthWordDateRange.startDate),
        maxDate: dateToUnixTimestamp(monthWordDateRange.endDate),
      };
    }

    // Filters.min_date and filters.max_date take precedence over filters.month_word
    if (filters.min_date || filters.max_date) {
      dateRange.minDate = filters?.min_date;
      dateRange.maxDate = filters?.max_date;
    }

    return query
      .andWhere('hacker_news.created_at_i >= :minDate', {
        minDate: dateRange?.minDate || 0,
      })
      .andWhere('hacker_news.created_at_i <= :maxDate', {
        maxDate: dateRange?.maxDate || Math.floor(new Date().getTime() / 1000),
      });
  }

  private addAuthorFilterToQuery(
    query: SelectQueryBuilder<HackerNews> | UpdateQueryBuilder<HackerNews>,
    filters?: QueryFiltersDto,
  ) {
    if (filters.author) {
      return query.where('LOWER(hacker_news.author) = LOWER(:author)', {
        author: filters.author,
      });
    }

    return query;
  }

  private addTagsFilterToQuery(
    query: SelectQueryBuilder<HackerNews> | UpdateQueryBuilder<HackerNews>,
    filters?: QueryFiltersDto,
  ) {
    if (filters.tags) {
      for (const tag of filters.tags) {
        query = query.andWhere(':tag ILIKE ANY(hacker_news._tags)', {
          tag,
        });
      }

      return query;
    }

    return query;
  }
}
