/* eslint-disable @typescript-eslint/no-empty-function */
import { HttpModule } from '@nestjs/axios';
import { Test } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { HackerNews } from '../entities/news.entity';
import { HackerNewsApiWrapper } from '../providers/hackerNewsApiWrapper.service';
import { NewsRepositoryService } from '../repository/newsRepository.service';

// eslint-disable-next-line @typescript-eslint/no-var-requires
require('dotenv').config({ path: '.env' });

describe('NewsRepositoryService', () => {
  let newsRepositoryService: NewsRepositoryService;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [
        HttpModule.register({ timeout: 5000 }),
        // TypeOrmModule.forFeature([HackerNews]),
      ],
      providers: [
        NewsRepositoryService,
        HackerNewsApiWrapper,
        {
          provide: getRepositoryToken(HackerNews),
          useValue: {
            clear: jest.fn(),
            insert: jest.fn(),
            createQueryBuilder: jest.fn(() => ({
              getMany: jest.fn(() => []),
              execute: jest.fn(() => ({ affected: 0 })),
              andWhere: jest.fn(),
              where: jest.fn(),
            })),
          },
        },
      ],
    }).compile();

    newsRepositoryService = moduleRef.get<NewsRepositoryService>(
      NewsRepositoryService,
    );
  });

  it('should be defined', () => {
    expect(newsRepositoryService).toBeDefined();
  });

  describe('findMany', () => {
    it('should be defined', () => {
      expect(newsRepositoryService.findMany).toBeDefined();
    });

    describe('changeNewsVisibility', () => {
      it('should be defined', () => {
        expect(newsRepositoryService.changeNewsVisibility).toBeDefined();
      });
    });

    describe('getHiddenRecords', () => {
      it('should be defined', () => {
        expect(newsRepositoryService.getHiddenRecords).toBeDefined();
      });
    });
  });
});
