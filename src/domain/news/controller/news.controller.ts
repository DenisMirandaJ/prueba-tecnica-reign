import { Controller, Get, Post, Query } from '@nestjs/common';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { PaginatedResponse } from '../../shared/interfaces/interfaces';
import {
  QueryFiltersDto,
  ShowHiddenRecordsOptionsDto,
  VisibilityQueryFiltersDto,
} from '../dto/newsFilters.dto';
import { HackerNews } from '../entities/news.entity';
import { NewsService } from '../providers/news.service';

@Controller()
@ApiTags('News')
export class NewsController {
  constructor(private readonly newsService: NewsService) {}

  @Get('search')
  findAll(
    @Query() searchQuery: QueryFiltersDto,
  ): Promise<PaginatedResponse<HackerNews>> {
    return this.newsService.findMany(searchQuery);
  }

  @ApiOperation({
    summary:
      'Hide the records matching the provided filters, they WILL NOT appear in subsequents calls to the /search endpoint',
  })
  @ApiResponse({ status: 201, description: 'OK' })
  @Post('visibility/hide')
  hideMany(@Query() searchQuery: VisibilityQueryFiltersDto) {
    return this.newsService.changeNewsVisibility(searchQuery, 'hide');
  }

  @ApiOperation({
    summary:
      'Show the records matching the provided filters, they WILL appear in subsequents calls to the /search endpoint',
  })
  @ApiResponse({ status: 201, description: 'OK' })
  @Post('visibility/show')
  showMany(@Query() searchQuery: VisibilityQueryFiltersDto) {
    return this.newsService.changeNewsVisibility(searchQuery, 'show');
  }

  @ApiOperation({
    summary: 'show a list of all the hidden records',
  })
  @ApiResponse({ status: 201, description: 'OK' })
  @Get('search/hidden')
  showHiddenRecords(@Query() searchQuery: ShowHiddenRecordsOptionsDto) {
    return this.newsService.getHiddenRecords(searchQuery);
  }
}
