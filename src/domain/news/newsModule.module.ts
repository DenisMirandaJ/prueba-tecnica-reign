import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { NewsRepositoryService } from './repository/newsRepository.service';
import { HackerNewsApiWrapper } from './providers/hackerNewsApiWrapper.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { HackerNews } from './entities/news.entity';
import { NewsController } from './controller/news.controller';
import { NewsService } from './providers/news.service';
import { NewsLoadTimes } from './entities/newsLoadTimes.entity';

@Module({
  imports: [
    HttpModule.register({ timeout: 5000 }),
    TypeOrmModule.forFeature([HackerNews, NewsLoadTimes]),
  ],
  controllers: [NewsController],
  providers: [NewsRepositoryService, HackerNewsApiWrapper, NewsService],
})
export class NewsModule {}
