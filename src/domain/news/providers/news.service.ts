import { Inject, Injectable, Optional } from '@nestjs/common';
import { Cron, CronExpression, Timeout } from '@nestjs/schedule';
import { WINSTON_MODULE_PROVIDER } from 'nest-winston';
import { Logger } from 'winston';
import TypeOrmExceptionHandler from '../../shared/exceptionsHandlers/typeormExceptionHandler';
import {
  QueryFiltersDto,
  ShowHiddenRecordsOptionsDto,
  VisibilityQueryFiltersDto,
} from '../dto/newsFilters.dto';
import { NewsRepositoryService } from '../repository/newsRepository.service';

@Injectable()
export class NewsService {
  constructor(
    private newsRepository: NewsRepositoryService,
    @Optional()
    @Inject(WINSTON_MODULE_PROVIDER)
    private readonly logger: Logger,
  ) {}

  @Cron(CronExpression.EVERY_HOUR)
  private async loadRecentHackerNewsIntoDb() {
    try {
      await this.newsRepository.saveRecentNews();
      this.logger.log('info', 'Loading the most recent hacker news');
    } catch (error) {
      TypeOrmExceptionHandler(error);
    }
  }

  @Timeout(0)
  private async loadRecentHackerNewsIntoDbAtStartup() {
    this.loadRecentHackerNewsIntoDb();
  }

  async findMany(filters: QueryFiltersDto) {
    try {
      return await this.newsRepository.findMany(filters);
    } catch (error) {
      TypeOrmExceptionHandler(error);
    }
  }

  async changeNewsVisibility(
    filters: VisibilityQueryFiltersDto,
    visibility: 'hide' | 'show',
  ) {
    try {
      return await this.newsRepository.changeNewsVisibility(
        filters,
        visibility,
      );
    } catch (error) {
      TypeOrmExceptionHandler(error);
    }
  }

  async getHiddenRecords(filters: ShowHiddenRecordsOptionsDto) {
    try {
      return await this.newsRepository.getHiddenRecords(filters);
    } catch (error) {
      TypeOrmExceptionHandler(error);
    }
  }
}
