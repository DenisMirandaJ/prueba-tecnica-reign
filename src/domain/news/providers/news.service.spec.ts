/* eslint-disable @typescript-eslint/no-empty-function */
import { HttpModule } from '@nestjs/axios';
import { Test } from '@nestjs/testing';
import { getRepositoryToken, TypeOrmModule } from '@nestjs/typeorm';
import { User } from '../../auth/entities/user.entity';
import { HackerNews } from '../entities/news.entity';
import { NewsRepositoryService } from '../repository/newsRepository.service';
import { HackerNewsApiWrapper } from './hackerNewsApiWrapper.service';
import { NewsService } from './news.service';

// eslint-disable-next-line @typescript-eslint/no-var-requires
require('dotenv').config({ path: '.env' });

describe('NewsService', () => {
  let newsService: NewsService;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [
        HttpModule.register({ timeout: 5000 }),
        // TypeOrmModule.forFeature([HackerNews]),
      ],
      providers: [
        NewsService,
        NewsRepositoryService,
        HackerNewsApiWrapper,
        {
          provide: getRepositoryToken(HackerNews),
          useValue: {
            clear: jest.fn(),
            insert: jest.fn(),
          },
        },
      ],
    }).compile();

    newsService = moduleRef.get<NewsService>(NewsService);
  });

  it('should be defined', () => {
    expect(newsService).toBeDefined();
  });

  describe('findMany', () => {
    it('should be defined', () => {
      expect(newsService.findMany).toBeDefined();
    });
  });

  describe('changeNewsVisibility', () => {
    it('should be defined', () => {
      expect(newsService.changeNewsVisibility).toBeDefined();
    });
  });

  describe('getHiddenRecords', () => {
    it('should be defined', () => {
      expect(newsService.getHiddenRecords).toBeDefined();
    });
  });
});
