import { HttpService } from '@nestjs/axios';
import { Injectable, ServiceUnavailableException } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import axios from 'axios';
import { apiRateLimiter } from '../../shared/utils/requestUtils';
import { HackerNewsResponse } from '../interfaces/hackerNewsApi.interfaces';

export interface HackerNewsApiWrapperConfig {
  createdAtUnixEpochInSeconds?: number; //Unix timestamp in seconds
  query?: string;
  page?: number;
}

Injectable();
export class HackerNewsApiWrapper {
  createdAt?: number;
  query?: string;
  page?: number;

  constructor(
    private readonly httpService: HttpService,
    private readonly configService: ConfigService,
  ) {}

  setParams(params: HackerNewsApiWrapperConfig) {
    this.createdAt = Math.floor(params.createdAtUnixEpochInSeconds);
    this.query = params.query;
    this.page = params.page || 0;
  }

  async *getApiResponse() {
    const responseFromApi = await this.loadNewsFromExternalApi();
    if (
      responseFromApi.nbPages === this.page ||
      responseFromApi.nbPages === 0
    ) {
      this.page = 0;
      return responseFromApi;
    }

    this.page = this.page + 1;
    yield responseFromApi;
  }

  private async loadNewsFromExternalApi(): Promise<HackerNewsResponse> {
    const numericFilters = this.createdAt
      ? `created_at_i>${this.createdAt}`
      : undefined;

    // Wrap the api call on a rate limiter of one request every 100ms, just to be good citizens
    const response = await apiRateLimiter.schedule(async () => {
      try {
        return axios.get<HackerNewsResponse>(process.env.HACKER_NEWS_API, {
          params: {
            query: this.query,
            page: this.page,
            numericFilters,
          },
        });
      } catch (error) {
        throw new ServiceUnavailableException(
          'Could not fetch from Hacker News API',
        );
      }
    });

    return response.data;
  }
}
