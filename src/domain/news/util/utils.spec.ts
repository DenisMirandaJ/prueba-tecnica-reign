import { transformSearchQueryTags } from './utils';

describe('newsUtils', () => {
  describe('transformSearchQueryTags', () => {
    it('should be defined', () => {
      expect(transformSearchQueryTags).toBeDefined();
    });

    it('should trasnsform tags filters corrrectly', () => {
      const tags = transformSearchQueryTags('tag1, tag2');
      expect(tags.length).toBe(2);
      expect(tags[0]).toBe('tag1');
      expect(tags[1]).toBe('tag2');
    });

    it('should trasnsform nested tags filters corrrectly', () => {
      const tags = transformSearchQueryTags('tag1, (tag2, tag3)');
      expect(tags.length).toBe(2);
      expect(tags[0]).toBe('tag1');
      expect(tags[1][0]).toBe('tag2');
      expect(tags[1][1]).toBe('tag3');
    });
  });
});
