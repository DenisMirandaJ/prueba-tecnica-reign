import { ApiPropertyOptional, OmitType, PickType } from '@nestjs/swagger';
import { Transform } from 'class-transformer';
import { IsString, IsOptional, IsNumber, IsIn } from 'class-validator';
import { MonthNameType, RecursiveArray } from '../../shared/types/types';
import { parseQueryNumber, transformSearchQueryTags } from '../util/utils';

export const monthIndex = {
  january: 0,
  february: 1,
  march: 2,
  april: 3,
  may: 4,
  june: 5,
  july: 6,
  august: 7,
  september: 8,
  october: 9,
  november: 10,
  december: 11,
};

export const monthNames = Object.keys(monthIndex);

export class QueryFiltersDto {
  @ApiPropertyOptional()
  @IsString()
  @IsOptional()
  author?: string;

  @ApiPropertyOptional({ type: 'string' })
  @Transform(({ value }) => transformSearchQueryTags(value))
  @IsOptional()
  tags?: RecursiveArray<string>;

  @ApiPropertyOptional({ description: 'UNIX Timestamp in seconds' })
  @Transform(({ value }) => parseInt(value))
  @IsNumber(
    { maxDecimalPlaces: 0 },
    { message: 'Must be a UNIX timestamp number' },
  )
  @IsOptional()
  min_date?: number; // Unix TimeStamp

  @ApiPropertyOptional({ description: 'UNIX Timestamp in seconds' })
  @Transform(({ value }) => parseInt(value))
  @IsNumber(
    { maxDecimalPlaces: 0 },
    { message: 'Must be a UNIX timestamp number' },
  )
  @IsOptional()
  max_date?: number; // Unix TimeStamp

  @ApiPropertyOptional()
  @IsString()
  @Transform(({ value }) => value.toLowerCase())
  @IsIn(monthNames)
  @IsOptional()
  month_word?: MonthNameType;

  @ApiPropertyOptional({ type: 'number' })
  @Transform(({ value }) => parseQueryNumber(value, { default: 1, min: 1 }))
  @IsNumber()
  page = 1;

  @ApiPropertyOptional({ type: 'number' })
  @Transform(({ value }) =>
    parseQueryNumber(value, { default: 5, max: 5, min: 1 }),
  )
  @IsNumber()
  items_per_page = 5;
}

export class VisibilityQueryFiltersDto extends OmitType(QueryFiltersDto, [
  'page',
  'items_per_page',
] as const) {}

export class ShowHiddenRecordsOptionsDto extends PickType(QueryFiltersDto, [
  'page',
  'items_per_page',
] as const) {}
