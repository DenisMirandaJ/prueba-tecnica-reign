import { NotFoundException, ServiceUnavailableException } from '@nestjs/common';
import { EntityNotFoundError, QueryFailedError } from 'typeorm';

const TypeOrmExceptionHandler = (error: unknown) => {
  if (error instanceof QueryFailedError) {
    throw new ServiceUnavailableException('No fue posible realizar la request');
  }

  if (error instanceof EntityNotFoundError) {
    throw new NotFoundException('Entidad no encontrada');
  }
};

export default TypeOrmExceptionHandler;
