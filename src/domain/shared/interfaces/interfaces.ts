export interface PaginatedResponse<T> {
  page: number;
  number_of_pages: number;
  items_per_page: number;
  items: T[];
}
