import { MonthNameType } from '../types/types';

interface DateRange {
  startDate: Date;
  endDate: Date;
}

export const getDateRangeByMonthName = (month: MonthNameType): DateRange => {
  const monthIndex = {
    january: 0,
    february: 1,
    march: 2,
    april: 3,
    may: 4,
    june: 5,
    july: 6,
    august: 7,
    september: 8,
    october: 9,
    november: 10,
    december: 11,
  };

  const currentDate = new Date();
  const currentYear = currentDate.getFullYear();

  let dateRange =
    month !== 'december'
      ? {
          startDate: new Date(currentYear, monthIndex[month], 1),
          endDate: new Date(currentYear, monthIndex[month] + 1, 0),
        }
      : {
          startDate: new Date(currentYear, 11, 1),
          endDate: new Date(currentYear, 11, 31),
        };

  if (dateRange.startDate > currentDate) {
    dateRange =
      month !== 'december'
        ? {
            startDate: new Date(currentYear - 1, monthIndex[month], 1),
            endDate: new Date(currentYear - 1, monthIndex[month] + 1, 0),
          }
        : {
            startDate: new Date(currentYear - 1, 11, 1),
            endDate: new Date(currentYear - 1, 11, 31),
          };
  }

  return dateRange;
};

export const dateToUnixTimestamp = (
  date: Date,
  timeUnit: 'SECONDS' | 'MILISECONDS' = 'SECONDS',
) => {
  if (timeUnit === 'SECONDS') {
    return date.getTime() / 1000;
  }

  return date.getTime();
};
