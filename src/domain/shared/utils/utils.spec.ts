import { getDateRangeByMonthName } from './date.utils';

describe('DateUtils', () => {
  describe('getDateRangeByMonthName', () => {
    it('should be defined', () => {
      expect(getDateRangeByMonthName).toBeDefined();
    });

    it('should return date range', () => {
      const dateRange = getDateRangeByMonthName('january');
      expect(dateRange.startDate).toBeInstanceOf(Date);
      expect(dateRange.endDate).toBeInstanceOf(Date);
    });
  });
});
