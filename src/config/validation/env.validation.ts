import { classToPlain, instanceToPlain, plainToClass } from 'class-transformer';
import {
  IsEnum,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
  validate,
  validateSync,
} from 'class-validator';

enum Environment {
  Development = 'development',
  Production = 'production',
}

export class EnviromentVariables {
  @IsEnum(Environment)
  NODE_ENV: Environment;

  @IsOptional()
  PORT?: number;

  @IsString()
  @IsNotEmpty()
  DB_HOST: string;

  @IsNotEmpty()
  @IsNumber()
  DB_PORT: number;

  @IsString()
  @IsNotEmpty()
  DB_USER: string;

  @IsString()
  @IsNotEmpty()
  DB_PASSWORD: string;

  @IsString()
  @IsNotEmpty()
  DB_NAME: string;

  @IsString()
  @IsNotEmpty()
  JWT_SECRET: string;
}

export const validateEnvObject = (
  envVars: Record<string, unknown>,
): Record<string, any> => {
  const validatedConfig = plainToClass(EnviromentVariables, envVars, {
    enableImplicitConversion: true,
  });

  const errors = validateSync(validatedConfig);
  if (errors.length > 0) {
    throw new Error(errors.toString());
  }
  console.log('validated');
  const a = instanceToPlain(validatedConfig);
  return instanceToPlain(validatedConfig);
};
