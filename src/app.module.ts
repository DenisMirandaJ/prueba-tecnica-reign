import { NewsModule } from './domain/news/newsModule.module';
import { MiddlewareConsumer, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule, ConfigService } from '@nestjs/config';
// import { validateEnvObject } from './config/validation/env.validation';
import { HackerNews } from './domain/news/entities/news.entity';
import { ScheduleModule } from '@nestjs/schedule';
import { AuthMiddleware } from './domain/auth/middlewares/auth.middleware';
import { NewsController } from './domain/news/controller/news.controller';
import { AuthModule } from './domain/auth/auth.module';
import { User } from './domain/auth/entities/user.entity';
import { validateEnvObject } from './config/validation/env.validation';
import { NewsLoadTimes } from './domain/news/entities/newsLoadTimes.entity';
import {
  WinstonModule,
  utilities as nestWinstonModuleUtilities,
} from 'nest-winston';
import * as winston from 'winston';
@Module({
  imports: [
    NewsModule,
    AuthModule,
    ScheduleModule.forRoot(),
    ConfigModule.forRoot({
      isGlobal: true,
      validate: validateEnvObject,
    }),
    WinstonModule.forRoot({
      level: 'info',
      transports: [
        new winston.transports.Console({
          format: winston.format.combine(
            winston.format.timestamp(),
            winston.format.ms(),
            nestWinstonModuleUtilities.format.nestLike('HackerNews', {
              // options
            }),
          ),
        }),
      ],
    }),
    TypeOrmModule.forRootAsync({
      inject: [ConfigService],
      useFactory: async (configService: ConfigService) => {
        await ConfigModule.envVariablesLoaded;
        return {
          type: 'postgres',
          host: configService.get('DB_HOST'),
          port: parseInt(configService.get('DB_PORT'), 10) || 5432,
          username: configService.get('DB_USER'),
          password: configService.get('DB_PASSWORD'),
          database: configService.get('DB_NAME'),
          entities: [HackerNews, User, NewsLoadTimes],
          synchronize: true,
          // logging: true,
        };
      },
    }),
  ],
  controllers: [],
  providers: [],
})
export class AppModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(AuthMiddleware).forRoutes(NewsController);
  }
}
